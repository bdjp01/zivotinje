/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : zivotinje

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 23/06/2019 01:11:21
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ad
-- ----------------------------
DROP TABLE IF EXISTS `ad`;
CREATE TABLE `ad`  (
  `ad_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pet_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `race` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `age` int(10) NOT NULL,
  `image_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `is_active` bit(1) NOT NULL,
  PRIMARY KEY (`ad_id`) USING BTREE,
  INDEX `fk_ad_user_id`(`user_id`) USING BTREE,
  INDEX `fk_ad_category_id`(`category_id`) USING BTREE,
  CONSTRAINT `fk_ad_category_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_ad_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ad
-- ----------------------------
INSERT INTO `ad` VALUES (1, 'Dajem malog simu na udomljavanje', 'Simo', 'pas', 'musko', 7, 'images/1.jpg', 'bjeli', 'Mali Simo za udomljavanje, prelep i umiljat pas.', 'Ustanicka 121', 1, 1, b'1');
INSERT INTO `ad` VALUES (2, 'Mali savo', 'Mima', 'Sibirska', 'zensko', 2, 'images/jpg.1', 'bleki', 'Lepa maca, ceka dom.', 'Vojvode Stepe 737', 2, 11, b'1');
INSERT INTO `ad` VALUES (3, '', 'Mila', 'Bengalska', 'zensko', 14, '', 'siva', 'Macka Mila ceka novog vlasnika, umiljata voli decu.', 'Radovana Simica 87', 2, 4, b'0');
INSERT INTO `ad` VALUES (4, '', 'Ruza', 'Sijamska', 'zensko', 3, '', 'siva', 'Ko zeli macu za svoj dom macka Ruza je pravi izbor. Kontaktirajte nas za vise detalja :)', 'Kaludjerica', 2, 4, b'0');
INSERT INTO `ad` VALUES (5, '', 'Roki', 'Sibirska', 'musko', 2, '', 'siva', 'Fini macak trazi dom.', 'Medakovic 3', 2, 5, b'0');
INSERT INTO `ad` VALUES (6, '', 'Bimbo', 'Persijska', 'musko', 6, '947.jpg', 'siva', 'Macak Bimbo ceka novog vlasnika.', 'Brace Jerkovic', 2, 6, b'0');
INSERT INTO `ad` VALUES (7, '', 'Milo', 'Bengalska', 'musko', 12, '712.jpg', 'siva', 'Milo je predivan macak, koji je pronadjen napusten u kutiji i od tada ceka novog vlasnika.', 'Novi Beograd', 2, 7, b'0');
INSERT INTO `ad` VALUES (8, '', 'Robi', 'Ovcar', 'musko', 12, '509.jpg', 'plavkasta', 'Robi je nemacki ovcar i ceka novu porodicu koja ce se brinuti o njemu.', 'Milesevska bb', 1, 8, b'0');
INSERT INTO `ad` VALUES (9, '', 'Lisa', 'Sibirska', 'zensko', 11, '249.jpg', 'CRVENA', 'Sibirka macka Lisa ceka dom.', 'Vitranovacka 231', 2, 9, b'0');
INSERT INTO `ad` VALUES (10, '', 'Luna', 'Sibirska', 'zensko', 5, '289.jpg', 'CRVENA', 'imam jednu macku', 'Milke Grgurove 14', 2, 10, b'0');
INSERT INTO `ad` VALUES (11, '', 'Laza', 'Ruska bela', 'musko', 1, '828.jpg', '123', 'Lepa maca, ceka dom.', 'Altina, Zemun', 2, 10, b'0');
INSERT INTO `ad` VALUES (12, '', 'Micko', 'Sijamska', 'musko', 14, '177.jpg', '123', 'Maca za udomljavanje', 'Zemun', 2, 4, b'0');
INSERT INTO `ad` VALUES (13, '', 'Sima', 'Ruska plava', 'musko', 12, '523.jpg', 'plavkasta', 'Ruski plavi macor ceka dom. Macak ima 12 godina ali je jos uvek kao decak.', 'Mirjevo', 2, 1, b'0');
INSERT INTO `ad` VALUES (14, '', 'Dule', 'Mesovita', 'musko', 2, '711.jpg', 'plavkasta', 'Lepa maca, ceka dom. Ne znamo koja je rasa, samo znamo da je preslaka i umiljata.', 'Vracar', 2, 8, b'0');
INSERT INTO `ad` VALUES (15, '', 'Kiza', 'Veverica', 'musko', 3, '142.jpg', 'Sivkasto smedja', 'Rasu imam jos od kad sam bio mali, sladak je, voli lesnike i voli da se igra sa decom.', 'Dorcol', 3, 2, b'0');
INSERT INTO `ad` VALUES (16, '', 'Laki', 'Veverica', 'musko', 3, '729.jpg', 'Sivkasto smedja', 'Rasu imam jos od kad sam bio mali, sladak je, voli lesnike i voli da se igra sa decom.', 'Pere Perica 23bb', 3, 7, b'0');
INSERT INTO `ad` VALUES (17, '', 'Buba', 'Labrador', 'zensko', 3, '322.jpg', 'Bela', 'Buba je divna kuca bele dlake. Umiljata je i voli decu.', 'Smederevo', 1, 16, b'0');
INSERT INTO `ad` VALUES (18, '', 'Cupko', 'Mesanac', 'musko', 11, '904.jpg', 'sivo-bela', 'Lep i cupav pas ceka dom.', 'Lozovik', 1, 16, b'0');
INSERT INTO `ad` VALUES (19, '', 'Fifi', 'Pudlica', 'zensko', 12, '772.jpg', 'bela', 'Fifi je slatka kuca i ceka udomljavanje', 'Lozovik', 1, 16, b'0');
INSERT INTO `ad` VALUES (20, '', 'Gagi', 'Bernardinac', 'musko', 12, '741.jpg', 'sivo-bela', 'Lep i cupav pas ceka dom', 'Lozovik', 1, 16, b'0');
INSERT INTO `ad` VALUES (21, '', 'Sara', 'Mops', 'zensko', 2, '772.jpg', 'smedja-kakao', 'Mopsic najbolji pas nadjen napusten, ceka dom', 'Pejicevi Salasi', 1, 16, b'0');
INSERT INTO `ad` VALUES (22, '', 'Stojan', 'Maltezer', 'musko', 5, '714.jpg', 'zuckasta', 'Prelep pas, pronadjen u parku', 'Zvezdara', 1, 16, b'0');
INSERT INTO `ad` VALUES (23, '', 'Lara', 'Retriver', 'zensko', 3, '472.jpg', 'zuckastobela', 'Prelep pas, pronadjen u parku', 'Zvezdara', 1, 16, b'0');
INSERT INTO `ad` VALUES (24, '', 'Stojan', 'ne znam', 'musko', 7, '236.jpg', 'zuckasta', 'Prelep pas, pronadjen u parku', 'Zvezdara', 1, 16, b'0');
INSERT INTO `ad` VALUES (25, '', 'Rea', 'Nisam siguran', 'zensko', 4, '529.jpg', 'smedja sa crno belim sarama', 'Divna kuca ceka dom', 'Selevac', 1, 16, b'0');
INSERT INTO `ad` VALUES (26, '', 'Rasta', 'ne znam', 'musko', 4, '193.jpg', 'smedja, sa belim stomakom', 'Divna kuca', 'Jagnjilo', 1, 16, b'0');
INSERT INTO `ad` VALUES (27, '', 'Loki', 'mesanac', 'musko', 1, '153.jpg', 'smedja', 'Malo kucence', 'Nis', 1, 16, b'0');
INSERT INTO `ad` VALUES (28, '', 'Arkan', 'Buldog', 'musko', 3, '242.jpg', 'belo--smedja', 'Lep i dresiran buldog', 'Umcari', 1, 16, b'0');
INSERT INTO `ad` VALUES (29, '', 'Riko', 'Papagaj', 'musko', 4, '282.jpg', 'zelena', 'Papagaj trazi kafez', 'Saraorci', 3, 16, b'0');
INSERT INTO `ad` VALUES (30, '', 'Janko', 'Puz', 'zensko', 9, '108.jpg', 'braon', 'Lep puz', 'Mala Krsna', 3, 16, b'0');
INSERT INTO `ad` VALUES (31, '', 'Kiki', 'Beli pacov', 'zensko', 1, '885.jpg', 'bela', 'pacovic', 'Ralja', 3, 16, b'0');
INSERT INTO `ad` VALUES (32, '', 'Pepi', 'Prase', 'musko', 1, '720.jpg', 'roze', 'Divno prasence ceka dom do Bozica', 'Golobok', 3, 16, b'0');
INSERT INTO `ad` VALUES (33, '', 'Leon', 'Zmija-za kucu', 'musko', 3, '598.jpg', 'sarena', 'Udomite zmiju da ne ode u zooloski vrt.', 'Mihajlovac', 3, 16, b'0');
INSERT INTO `ad` VALUES (34, '', 'Miki', 'Retriver', 'musko', 2, '981.jpg', 'bela', 'Ovo je pas Miki koji ceka novi dom', 'Beograd', 1, 17, b'0');

-- ----------------------------
-- Table structure for ad_view
-- ----------------------------
DROP TABLE IF EXISTS `ad_view`;
CREATE TABLE `ad_view`  (
  `ad_view_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `ad_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(24) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ad_view_id`) USING BTREE,
  INDEX `fk_ad_view_ad_id`(`ad_id`) USING BTREE,
  INDEX `ad_view_ip_address_idx`(`ip_address`) USING BTREE,
  CONSTRAINT `fk_ad_view_ad_id` FOREIGN KEY (`ad_id`) REFERENCES `ad` (`ad_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 334 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ad_view
-- ----------------------------
INSERT INTO `ad_view` VALUES (1, '2019-06-10 15:14:32', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (2, '2019-06-10 15:14:33', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (3, '2019-06-10 15:14:33', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (4, '2019-06-10 23:26:52', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (5, '2019-06-10 23:26:53', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (6, '2019-06-10 23:26:59', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (7, '2019-06-10 23:27:04', 1, '46.240.187.92', 'chrome/123312/asdsad');
INSERT INTO `ad_view` VALUES (8, '2019-06-13 14:45:40', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (9, '2019-06-13 14:45:43', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (10, '2019-06-13 14:57:56', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (11, '2019-06-13 14:59:08', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (12, '2019-06-13 15:09:52', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (13, '2019-06-13 15:11:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (14, '2019-06-13 15:11:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (15, '2019-06-13 15:11:32', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (16, '2019-06-13 15:11:33', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (17, '2019-06-13 15:11:33', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (18, '2019-06-13 15:11:33', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (19, '2019-06-13 15:11:33', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (20, '2019-06-13 15:11:33', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (21, '2019-06-13 15:11:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (22, '2019-06-13 15:11:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (23, '2019-06-13 15:11:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (24, '2019-06-13 15:11:34', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (25, '2019-06-13 15:11:35', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (26, '2019-06-13 15:11:35', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (27, '2019-06-13 15:11:35', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (28, '2019-06-13 15:11:59', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (29, '2019-06-13 15:12:01', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (30, '2019-06-13 15:12:18', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (31, '2019-06-13 15:12:25', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (32, '2019-06-13 15:24:46', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (33, '2019-06-13 15:25:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (34, '2019-06-13 15:25:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (35, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (36, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (37, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (38, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (39, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (40, '2019-06-13 15:25:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (41, '2019-06-13 15:25:20', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (42, '2019-06-13 15:29:06', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (43, '2019-06-13 15:29:27', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (44, '2019-06-13 15:29:28', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (45, '2019-06-13 15:29:28', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (46, '2019-06-13 15:29:28', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (47, '2019-06-13 15:29:29', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (48, '2019-06-13 15:29:29', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (49, '2019-06-13 15:29:29', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (50, '2019-06-13 15:29:29', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (51, '2019-06-13 15:29:29', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (52, '2019-06-13 15:31:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (53, '2019-06-13 15:31:06', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (54, '2019-06-13 15:31:06', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (55, '2019-06-13 15:31:06', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (56, '2019-06-13 15:31:07', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (57, '2019-06-13 15:31:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (58, '2019-06-13 15:31:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (59, '2019-06-13 15:31:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (60, '2019-06-13 15:31:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (61, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (62, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (63, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (64, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (65, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (66, '2019-06-13 15:31:26', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (67, '2019-06-13 15:31:58', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (68, '2019-06-13 15:31:59', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (69, '2019-06-13 15:32:00', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (70, '2019-06-13 15:32:00', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (71, '2019-06-13 15:32:01', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (72, '2019-06-13 15:32:09', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (73, '2019-06-13 15:32:09', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (74, '2019-06-13 15:32:09', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (75, '2019-06-13 15:32:10', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (76, '2019-06-13 15:32:21', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (77, '2019-06-13 15:32:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (78, '2019-06-13 15:32:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (79, '2019-06-13 15:32:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (80, '2019-06-13 15:32:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (81, '2019-06-13 15:32:32', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (82, '2019-06-13 15:32:42', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (83, '2019-06-13 15:32:52', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (84, '2019-06-13 15:32:53', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (85, '2019-06-13 15:33:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (86, '2019-06-13 15:33:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (87, '2019-06-13 15:33:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (88, '2019-06-13 15:34:07', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (89, '2019-06-13 15:34:43', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (90, '2019-06-13 15:34:43', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (91, '2019-06-13 15:34:43', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (92, '2019-06-13 15:34:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (93, '2019-06-13 15:34:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (94, '2019-06-13 15:34:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (95, '2019-06-13 15:34:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (96, '2019-06-13 15:34:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (97, '2019-06-13 15:34:45', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (98, '2019-06-13 15:34:45', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (99, '2019-06-13 15:34:46', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (100, '2019-06-13 15:34:47', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (101, '2019-06-13 15:34:50', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (102, '2019-06-13 15:36:01', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (103, '2019-06-13 15:36:21', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (104, '2019-06-13 15:36:22', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (105, '2019-06-13 15:36:23', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (106, '2019-06-13 15:36:24', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (107, '2019-06-13 15:36:42', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (108, '2019-06-13 15:36:43', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (109, '2019-06-13 15:36:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (110, '2019-06-13 15:36:45', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (111, '2019-06-13 15:36:45', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (112, '2019-06-13 15:36:46', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (113, '2019-06-13 18:28:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (114, '2019-06-13 18:28:47', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (115, '2019-06-13 18:39:44', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (116, '2019-06-13 18:40:06', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (117, '2019-06-13 18:40:49', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (118, '2019-06-13 21:13:38', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (119, '2019-06-13 21:14:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (120, '2019-06-13 21:33:53', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (121, '2019-06-13 22:02:13', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (122, '2019-06-13 22:02:24', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (123, '2019-06-13 22:02:29', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (124, '2019-06-13 22:03:06', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (125, '2019-06-13 22:20:30', 9, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (126, '2019-06-14 03:21:20', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (127, '2019-06-14 03:22:21', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (128, '2019-06-14 03:27:18', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (129, '2019-06-14 03:27:21', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (130, '2019-06-14 03:27:21', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (131, '2019-06-14 03:27:22', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (132, '2019-06-14 03:27:30', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (133, '2019-06-14 03:27:33', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (134, '2019-06-14 03:27:33', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (135, '2019-06-14 03:27:40', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (136, '2019-06-14 03:27:43', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (137, '2019-06-14 03:27:43', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (138, '2019-06-14 03:27:44', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (139, '2019-06-14 03:28:11', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (140, '2019-06-14 03:28:17', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (141, '2019-06-14 03:28:19', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (142, '2019-06-14 03:28:20', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (143, '2019-06-14 03:28:20', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (144, '2019-06-14 03:28:32', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (145, '2019-06-14 03:28:35', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (146, '2019-06-14 03:28:36', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (147, '2019-06-14 03:28:36', 10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (148, '2019-06-14 15:50:51', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (149, '2019-06-14 16:31:40', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (150, '2019-06-14 18:06:38', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (151, '2019-06-14 18:06:51', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (152, '2019-06-14 18:06:51', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (153, '2019-06-14 19:01:32', 5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (154, '2019-06-18 18:00:11', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');
INSERT INTO `ad_view` VALUES (155, '2019-06-22 15:13:11', 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (156, '2019-06-22 15:18:15', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (157, '2019-06-22 15:19:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (158, '2019-06-22 15:23:03', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (159, '2019-06-22 15:23:13', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (160, '2019-06-22 15:23:13', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (161, '2019-06-22 15:23:14', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (162, '2019-06-22 15:23:15', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (163, '2019-06-22 15:23:15', 16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (164, '2019-06-22 16:02:48', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (165, '2019-06-22 16:05:16', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (166, '2019-06-22 16:06:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (167, '2019-06-22 16:06:39', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (168, '2019-06-22 16:07:44', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (169, '2019-06-22 16:07:51', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (170, '2019-06-22 16:08:05', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (171, '2019-06-22 16:09:09', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (172, '2019-06-22 16:10:23', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (173, '2019-06-22 16:10:24', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (174, '2019-06-22 16:10:24', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (175, '2019-06-22 16:10:24', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (176, '2019-06-22 16:10:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (177, '2019-06-22 16:11:03', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (178, '2019-06-22 16:11:03', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (179, '2019-06-22 16:11:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (180, '2019-06-22 16:11:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (181, '2019-06-22 16:11:04', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (182, '2019-06-22 16:11:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (183, '2019-06-22 16:11:28', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (184, '2019-06-22 16:14:35', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (185, '2019-06-22 16:14:36', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (186, '2019-06-22 16:15:20', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (187, '2019-06-22 16:15:27', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (188, '2019-06-22 16:15:35', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (189, '2019-06-22 16:15:59', 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (190, '2019-06-22 16:16:37', 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (191, '2019-06-22 16:18:14', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (192, '2019-06-22 16:19:16', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (193, '2019-06-22 16:19:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (194, '2019-06-22 16:19:17', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (195, '2019-06-22 16:19:18', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (196, '2019-06-22 16:19:45', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (197, '2019-06-22 16:56:33', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (198, '2019-06-22 16:57:01', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (199, '2019-06-22 16:57:03', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (200, '2019-06-22 16:57:03', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (201, '2019-06-22 16:57:18', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (202, '2019-06-22 16:57:19', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (203, '2019-06-22 16:57:19', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (204, '2019-06-22 16:57:19', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (205, '2019-06-22 16:57:20', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (206, '2019-06-22 16:57:20', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (207, '2019-06-22 16:57:36', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (208, '2019-06-22 16:57:43', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (209, '2019-06-22 16:57:44', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (210, '2019-06-22 16:57:53', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (211, '2019-06-22 16:57:53', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (212, '2019-06-22 16:57:53', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (213, '2019-06-22 16:57:54', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (214, '2019-06-22 16:58:11', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (215, '2019-06-22 16:58:12', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (216, '2019-06-22 16:58:20', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (217, '2019-06-22 16:58:31', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (218, '2019-06-22 16:58:32', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (219, '2019-06-22 16:58:33', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (220, '2019-06-22 16:58:34', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (221, '2019-06-22 16:59:12', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (222, '2019-06-22 16:59:18', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (223, '2019-06-22 16:59:42', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (224, '2019-06-22 17:26:47', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (225, '2019-06-22 17:27:45', 3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (226, '2019-06-22 17:32:20', 3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (227, '2019-06-22 17:42:29', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (228, '2019-06-22 17:42:32', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (229, '2019-06-22 17:42:33', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (230, '2019-06-22 17:42:33', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (231, '2019-06-22 17:57:20', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (232, '2019-06-22 18:51:39', 33, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (233, '2019-06-22 19:06:40', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (234, '2019-06-22 19:07:02', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (235, '2019-06-22 19:07:03', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (236, '2019-06-22 19:07:03', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (237, '2019-06-22 19:07:03', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (238, '2019-06-22 19:07:28', 19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (239, '2019-06-22 19:08:05', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (240, '2019-06-22 19:08:12', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (241, '2019-06-22 19:08:18', 19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (242, '2019-06-22 19:11:23', 19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (243, '2019-06-22 19:11:31', 31, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (244, '2019-06-22 19:11:36', 21, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (245, '2019-06-22 19:11:40', 32, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (246, '2019-06-22 19:11:48', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (247, '2019-06-22 19:13:23', 5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (248, '2019-06-22 19:13:32', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (249, '2019-06-22 19:13:40', 3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (250, '2019-06-22 19:13:44', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (251, '2019-06-22 19:13:50', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (252, '2019-06-22 19:14:47', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (253, '2019-06-22 19:14:53', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (254, '2019-06-22 19:14:55', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (255, '2019-06-22 19:15:07', 19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (256, '2019-06-22 19:15:11', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (257, '2019-06-22 19:15:15', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (258, '2019-06-22 19:16:44', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (259, '2019-06-22 19:17:02', 3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (260, '2019-06-22 19:17:05', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (261, '2019-06-22 19:17:07', 5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (262, '2019-06-22 19:17:11', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (263, '2019-06-22 19:17:25', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (264, '2019-06-22 19:17:28', 6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (265, '2019-06-22 19:17:32', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (266, '2019-06-22 19:19:23', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (267, '2019-06-22 19:19:25', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (268, '2019-06-22 19:19:55', 2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (269, '2019-06-22 19:19:58', 4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (270, '2019-06-22 19:20:02', 5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (271, '2019-06-22 19:20:06', 7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (272, '2019-06-22 19:20:28', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (273, '2019-06-22 19:20:36', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (274, '2019-06-22 19:20:37', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (275, '2019-06-22 19:20:37', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (276, '2019-06-22 19:27:23', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (277, '2019-06-22 19:27:38', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (278, '2019-06-22 19:27:42', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (279, '2019-06-22 19:27:46', 20, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (280, '2019-06-22 19:28:00', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (281, '2019-06-22 19:28:05', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (282, '2019-06-22 19:28:46', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (283, '2019-06-22 19:28:49', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (284, '2019-06-22 19:55:41', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (285, '2019-06-22 19:55:44', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (286, '2019-06-22 19:55:48', 22, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (287, '2019-06-22 19:55:56', 26, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (288, '2019-06-22 19:56:19', 25, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (289, '2019-06-22 19:58:35', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (290, '2019-06-22 19:58:47', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (291, '2019-06-22 19:59:23', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (292, '2019-06-22 19:59:36', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (293, '2019-06-22 20:01:57', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (294, '2019-06-22 20:03:35', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (295, '2019-06-22 20:03:36', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (296, '2019-06-22 20:03:36', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (297, '2019-06-22 20:04:16', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (298, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (299, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (300, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (301, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (302, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (303, '2019-06-22 20:04:17', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (304, '2019-06-22 20:04:18', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (305, '2019-06-22 20:04:18', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (306, '2019-06-22 20:04:44', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (307, '2019-06-22 20:08:20', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (308, '2019-06-22 20:12:29', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (309, '2019-06-22 20:13:26', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (310, '2019-06-22 20:14:30', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (311, '2019-06-22 20:14:30', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (312, '2019-06-22 20:14:31', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (313, '2019-06-22 20:14:39', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (314, '2019-06-22 20:15:11', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (315, '2019-06-22 20:15:45', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (316, '2019-06-22 20:15:58', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (317, '2019-06-22 20:15:59', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (318, '2019-06-22 20:15:59', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (319, '2019-06-22 20:16:18', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (320, '2019-06-22 20:16:19', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (321, '2019-06-22 20:16:24', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (322, '2019-06-22 23:27:54', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (323, '2019-06-22 23:28:09', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (324, '2019-06-22 23:28:14', 8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (325, '2019-06-22 23:28:18', 17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (326, '2019-06-22 23:28:21', 18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (327, '2019-06-22 23:28:25', 20, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (328, '2019-06-22 23:28:28', 25, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (329, '2019-06-22 23:28:31', 23, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (330, '2019-06-22 23:28:33', 28, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (331, '2019-06-22 23:28:36', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (332, '2019-06-22 23:28:46', 34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');
INSERT INTO `ad_view` VALUES (333, '2019-06-22 23:28:57', 12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, 'Pas');
INSERT INTO `category` VALUES (2, 'Macka');
INSERT INTO `category` VALUES (3, 'Ostalo');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `forename` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` int(13) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Zivorad', 'Zikic', 'Zvornik', 'zzikic', '12345678', 60456547);
INSERT INTO `user` VALUES (2, 'Stanislav', 'Pancic', 'Orasac', 'spancic', '817230912730912', 643215966);
INSERT INTO `user` VALUES (3, 'Stevo', 'Djurovic', 'Kosovska Mitrovica', 'stevodj', '$2y$10$2p/f0rtya8mvatCaOX53ZumJ0zfTKz5HC5oWqoPp1jA.bg1Pw5VLu', 698796554);
INSERT INTO `user` VALUES (4, 'Milos', 'Milosevic', 'Subotica', 'mmilosevic', '$2y$10$vpCH79MeSPatEa5YAvDCouIEP3lUpbOLbQ0LMAN5P0gHu2VWglf8e', 697896541);
INSERT INTO `user` VALUES (5, 'Tasa', 'Tanasic', 'Subotica', 'ttanasic', '$2y$10$kdUhwGqCrTzP4q71Y/A6ieXFHzWjJpI4lTWfG3xPKxzmyBmN4MGA6', 656336456);
INSERT INTO `user` VALUES (6, 'Andrej', 'Andrejevic', 'Valjevo', 'aandrejevic', '$2y$10$hSDKWUjxQhkqFCXFdCxdfuFxNEipapN0rLh4OgQj9Z7VHHzQ92xBC', 214748364);
INSERT INTO `user` VALUES (7, 'Dejan', 'Vasic', 'Loznica', 'dvasic', '$2y$10$vdj/3pldY43EXWrdYhtPTuxka6hQT7OSc8t7H.D5cT7rSvvjJd.c.', 644566554);
INSERT INTO `user` VALUES (8, 'Ivana', 'Radovic', 'Bjelo Polje', 'iradovic', '$2y$10$m4Oev4yLipFx3ahPO0tL/.DAidGHtSp0B06qarYh9lVToxB2uUzSq', 622558852);
INSERT INTO `user` VALUES (9, 'Marinko ', 'Rokvic', 'Beograd', 'mrokvic', '$2y$10$/OIYVAjdNm.wNmNkZjGMIe1AGDY4IeBWYMD5G8tG7FsnxIbW6pKUK', 666362614);
INSERT INTO `user` VALUES (10, 'Djordje', 'Cvarkov', 'Pejicevi Salasi', 'cvarkov', '$2y$10$2y7rx2EvvgMkMyBWgXVjBu9Kw3/rIcB9AcaO1VZonjgmAuQvHtnMS', 679874565);
INSERT INTO `user` VALUES (11, 'Bosko', 'Boskic', 'Novi Sad', 'boskic', '$2y$10$NWWzSrtwiPzO.UfTWmQk7uJ504GFuff9i7d3Rafum.PIzRreH91PO', 64321458);
INSERT INTO `user` VALUES (16, 'David', 'Borikic', 'Smederevo', 'dborikic', '$2y$10$2p/f0rtya8mvatCaOX53ZumJ0zfTKz5HC5oWqoPp1jA.bg1Pw5VLu', 6798556);
INSERT INTO `user` VALUES (17, 'Bojan', 'Filipovic', 'Beograd', 'bfilipovic', '$2y$10$L2L6cZWGIu8hvz1v9T.ae.SQVVKLNGE/mBrZNEckxL9IkFTmTZ9PG', 61222222);

SET FOREIGN_KEY_CHECKS = 1;
