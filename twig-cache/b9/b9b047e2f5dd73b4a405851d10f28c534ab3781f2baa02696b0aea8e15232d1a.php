<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/postLogin.html */
class __TwigTemplate_28ee17984f903962b29f32428378429a6c574aca1622938aad14bd6f7e1be988 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "Main/postLogin.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<div class=\"container\">
    <div class=\"card text-white bg-dark my-5\" >
            <div class=\"row no-gutters\">
            <div class=\"col-md-4\">
                <img src=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/warning.png\" class=\"card-img p-5\" alt=\"upozorenje\">
            </div>
            <div class=\"col-md-8\">
                <div class=\"card-body mt-5\">
                <h3 class=\"card-title\">Greška</h3>
                <h4 class=\"card-text\">";
        // line 13
        echo twig_escape_filter($this->env, ($context["message"] ?? null));
        echo "</h4>
                <a href=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "/user/login\"><button class=\"btn btn-primary\">Vrati se nazad</button></a>
                </div>
            </div>
            </div>
        </div>
    </div>
    
";
    }

    // line 23
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "Login
";
    }

    public function getTemplateName()
    {
        return "Main/postLogin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 24,  81 => 23,  69 => 14,  65 => 13,  57 => 8,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/postLogin.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\Main\\postLogin.html");
    }
}
