<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _global/index.html */
class __TwigTemplate_7b0249ae989ace3fb9e73fd7fc4b9ab055cb606dba84a1677bc6dedb2948284a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'naslov' => [$this, 'block_naslov'],
            'welcome' => [$this, 'block_welcome'],
            'main' => [$this, 'block_main'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title> ";
        // line 4
        $this->displayBlock('naslov', $context, $blocks);
        echo "</title>
        <meta charset=\"utf-8\">
        <link rel=\"shortcut icon\" type=\"image/png\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/logo.png\">
        <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/css/bootstrap.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/font-awesome/css/font-awesome.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/@fortawesome/fontawesome-free/css/all.min.css\">
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/css/style.css?ts=<?=time()?>\">
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/css/main.css?ts=<?=time()?>\">
\t    
    </head>
    
    <body data-spy=\"scroll\" data-target=\"#navbarResponsive\">
        
        <!-- Start home Section -->
            <div class=\"container-fluid\" >
        
        <!-- Navigation menu -->\t\t
            <nav class=\"navbar navbar-expand-md navbar-dark bg-dark\">
                <a class=\"navbar-brand\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/logo.png\"></a>
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
                
        
                <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">
                        ";
        // line 29
        $this->displayBlock('welcome', $context, $blocks);
        // line 30
        echo "                    <ul class=\"navbar-nav ml-auto\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\">Početna</a>
                        </li>
        
                        <li class=\"nav-item dropdown\">
                          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\"     data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Udomi</a>  
                          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">  
                            <a class=\"dropdown-item\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "ads/dogs/\">Psa</a>  
                            <a class=\"dropdown-item\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "ads/cats/\">Mačku</a>  
                            <a class=\"dropdown-item\" href=\"";
        // line 40
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "ads/other/\">Ostale životinje</a>  
                          </div>
                        </li>
        
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"";
        // line 45
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "about\">O nama</a>
                        </li>
                        <li class=\"nav-item\">
                                <a class=\"nav-link\" href=\"";
        // line 48
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/dashboard\">Moj profil</a>
                            </li>
                        <li class=\"nav-item\">
                                    <a class=\"nav-link\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/logout\">Odjava</a>
                            </li>
                        <a id=\"kreiraj\" href=\"";
        // line 53
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/login\"><button type=\"button\" class=\"btn btn-info\" id=\"prijava1\">Prijavi se / Kreiraj nalog</button></a>
                    </ul>
                </div>
            </nav>\t
            <!-- End of navigation section -->
            
            







            <div>
                ";
        // line 68
        $this->displayBlock('main', $context, $blocks);
        // line 71
        echo "            </div>
            
        
            
        
        
        
        <!-- Start Contact Section -->
            <div id=\"contact\" class=\"offset\">
                
                <footer>
                    <div class=\"row justify-content-center\">
                        <div class=\"col-md-5 text-center\">
                            <img href=\"";
        // line 84
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" src=\"";
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/logo.png\">
                            <p>UdomiMe.com je besplatan web portal gde možete besplatno postaviti i udomiti ljubimce.</p>
                            <br>
                            <strong>Kontakt informacije</strong>
                            <p>+381 60066006 <br>udomi@udomime.rs</p>
                            <a href=\"";
        // line 89
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fab fa-facebook-square fa-2x mx-3\"></i></a>
                            <a href=\"";
        // line 90
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fab fa-instagram fa-2x mx-3\"></i></a>
                            <a href=\"";
        // line 91
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" target=\"_blank\"><i class=\"fa fa-home fa-2x mx-3\"></i></a>
                        </div>
        
                        <hr class=\"socket\">
                        &copy; Studenti Univeziteta Singidunum
                    </div>
                </footer>
        
            </div>
           </div>     

        <script>const BASE = '";
        // line 102
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "';</script>
        <script src=\"";
        // line 103
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/jquery/dist/jquery.min.js\"></script>
        <script src=\"";
        // line 104
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/libs/bootstrap/dist/js/bootstrap.min.js\"></script>
        
    </body>
</html>";
    }

    // line 4
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "Aukcije";
    }

    // line 29
    public function block_welcome($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo " ";
    }

    // line 68
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 69
        echo "                
                ";
    }

    public function getTemplateName()
    {
        return "_global/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 69,  241 => 68,  234 => 29,  227 => 4,  219 => 104,  215 => 103,  211 => 102,  197 => 91,  193 => 90,  189 => 89,  179 => 84,  164 => 71,  162 => 68,  144 => 53,  139 => 51,  133 => 48,  127 => 45,  119 => 40,  115 => 39,  111 => 38,  102 => 32,  98 => 30,  96 => 29,  84 => 22,  70 => 11,  66 => 10,  62 => 9,  58 => 8,  54 => 7,  50 => 6,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "_global/index.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\_global\\index.html");
    }
}
