<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/home.html */
class __TwigTemplate_1b4f639077dca30941207d68667e5a076705e12a0b026471a3270c8151bc4ce2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "Main/home.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <!-- Start landing page -->
    
    <div id=\"carouselExampleInterval\" class=\"carousel d-block w-100 slide\" data-ride=\"carousel\">
    
            <div class=\"carousel-inner\" >
              <div class=\"carousel-item active\" data-interval=\"2000\">
                <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/banner-1.png\" class=\"d-block w-100\" alt=\"...\">
              </div>
              <div class=\"carousel-item\" data-interval=\"2000\">
                <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/banner-2.png\" class=\"d-block w-100\" alt=\"...\">
              </div>
              <div class=\"carousel-item\" data-interval=\"2000\">
                <img src=\"";
        // line 16
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/banner-3.png\" class=\"d-block w-100\" alt=\"...\">
              </div>
            </div>
            <a class=\"carousel-control-prev\" href=\"#carouselExampleInterval\" role=\"button\" data-slide=\"prev\">
                    <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Previous</span>
                  </a>
                  <a class=\"carousel-control-next\" href=\"#carouselExampleInterval\" role=\"button\" data-slide=\"next\">
                    <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">Next</span>
                  </a>
          </div>
    <!-- End Home section -->
    
    
    <!-- Start Break-sec Section -->
        <div id=\"break-sec\" class=\"offset\">
            <div class=\"col-12 narrow text-center\">
                <h1>UdomiMe.rs je besplatan web portal gde možete besplatno postaviti i udomiti ljubimce.</h1>
                
                <a class=\"btn btn-secondary btn-md\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/register\" target=\"_blank\">Registruj se besplatno</a>
            </div>
        </div>
    <!-- End Break-sec section -->
    
    
    
    
    <!-- Start Features Section -->
        <div id=\"features\" class=\"offset\">
            <!-- Start Jumbotron -->
            <div class=\"jumbotron\">
                <div class=\"narrow text-center\">
                    <div class=\"col-12\">
                        <h3 class=\"heading\">Ljubimci</h3>
                        <div class=\"heading-underline\"></div>
                    </div>
    
                    <div class=\"row justify-content-around kolona\">
                            ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, ($context["oglasi"] ?? null), 0, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["oglas"]) {
            // line 56
            echo "                            <div class=\"col-md-3 px-5 mb-5\">
                                    <div class=\"card text-center text-white \" id=\"boja-kartice\">
                                        <div class=\"card-header\"><h3>";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["oglas"], "pet_name", [], "any", false, false, false, 58));
            echo "</h3> </div>
                                        <img id=\"slika-oglas\" src=\"";
            // line 59
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "assets/uploads/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["oglas"], "ad_id", [], "any", false, false, false, 59), "html", null, true);
            echo ".jpg\" class=\"card-img-top\" alt=\"...\">
                                        <div class=\"card-body\">
                                          <h5 class=\"card-title\"><b class=\"float-left\">Starost:</b> <b class=\"float-right\">";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["oglas"], "age", [], "any", false, false, false, 61));
            echo " godina</b></h5><br>
                                          <h5 class=\"card-title\"><b class=\"float-left\">Rasa:</b> <b class=\"float-right\">";
            // line 62
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["oglas"], "race", [], "any", false, false, false, 62));
            echo "</b></h5>
                                       
                                        </div>
                                        <div class=\"card-footer\">
                                            <a href=\"";
            // line 66
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "/ads/pet/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["oglas"], "ad_id", [], "any", false, false, false, 66), "html", null, true);
            echo "\"> <button type=\"button\" class=\"btn btn-success btn-block dugme-oglas\">Vidi oglas </button></a>
                                        </div>
                                      </div>
                                </div>
                      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oglas'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                    </div>
    
                </div><!-- end narrow -->
            </div><!-- end Jumbotron -->
    
    
        </div>
    <!-- End Features section -->
    
        
    <!-- Start o-nama Section -->
        <div id=\"o-nama\" class=\"offset\">
            
            <div class=\"fixed-background\">
                <div class=\"row dark text-center\">
                    <div class=\"col-12\">
                        <h3 class=\"heading\">O nama</h3>
                        <p class=\"p-onama\">UdomiMe.rs je neprofitabilna zajednica napravljena sa ciljem da na jednom mestu objedini sve one u Srbiji koji su uključeni u brigu i zaštitu životinja. Bilo da ste vlasnik neke životinje ili želite da je usvojite, bilo da vam je potrebna pomoć ili želite i sami da pomognete ova stranica vam može pomoći da dodjete do informacija ili uspostavite kontakte.</p>
                        <div class=\"heading-underline\"></div>
                    </div>
    
    
                    <div class=\"col-md-4\">
                        <div class=\"feature\">
                            <i class=\"fa fa-paw fa-2x mb-3\"></i>
                        </div>
                        <h3>Udomljavanje</h3>
                        
                        <p class=\"lead\">Budite humani i pružite dom onima koji ga nemaju. Ukoliko se odlučite da postanete vlasnik kućnog ljubimca, razmotrite mogućnost udomljavanja napuštenih i nezbrinutih životinja. Ove životinje zaslužuju topao dom i neizmernu ljubav koju će Vam sigurno uzvratiti.</p>
                    </div>
    
                    <div class=\"col-md-4\">
                        <div class=\"feature\">
                                <i class=\"fas fa-hand-holding-usd fa-2x mb-3\"></i>
                        </div>
                        <h3>Doniraj</h3>
                        
                        <p class=\"lead\">Nedostatak finansija za odgovarajuću brigu i negu neudomljenih životinja jedan je od najvećih problema sa kojim se suočava svako udruženje za zaštitu životinja u Srbiji. Ako mislite da nemate dovoljno sredstava za donaciju, setite se da nijedna donacija nije suviše mala da bi pomogla.</p>
                    </div>
    
                    <div class=\"col-md-4\">
                        <div class=\"feature\">
                            <i class=\"fas fa-hands-helping fa-2x mb-3\"></i>
                        </div>
                        <h3>Volontiraj</h3>
                        
                        <p class=\"lead\">Volite životinje, kreativni ste, želite da pomognete i kvalitetno utrošite svoje slobodno vreme, ili ste možda oduvek želeli da imate neku životinju ali to niste bili u mogućnosti. Kontaktirajte nas i postanite deo našeg tima</p>
                    </div>
    
                </div><!-- end row dark -->
    
                <div class=\"fixed-wrap\">
                    <div class=\"fixed\">
                        
                    </div>
                </div>
            </div><!-- end of fixed-background -->
    
    
        </div>
    <!-- End o-nama section -->
    
    
    
    <!-- Start Clients Section -->
        <div id=\"clients\" class=\"offset\">
            <!-- Start jumbotron -->
            <div class=\"jumbotron\">
    
                <div class=\"col-12 text-center\">
                    <h3 class=\"heading\">Šta kažu oni koji su udomili neku životinju</h3>
                    <div class=\"heading-underline\"></div>
                </div>
    
                <div class=\"row\">
                    <div class=\"col-md-6 clients\">
                        <div class=\"row\">
                            <div class=\"col-md-4\">
                                <img class=\"img-thumbnail\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/stana.jpg\">
                            </div>
    
                            <div class=\"col-md-8\">
                                <blockquote>
                                    <i class=\"fa fa-quote-left\"></i>
                                    Želeli smo da da u svoj dom uvedemo nekog novog, i odlučili smo se za psa Reksa kog smo udomili preko sajta UdomiMe.rs. Reks se sjajno uklopio i planiramo da udomimo jos jednu macu :) 
                                    <hr class=\"clients-hr\">
                                    <cite>&#8212; Stana, modelsica</cite>
                                </blockquote>
                            </div>
                        </div>
                    </div>
    
    
                    <div class=\"col-md-6 clients\">
                        <div class=\"row\">
                            <div class=\"col-md-4\">
                                <img class=\"img-thumbnail\" src=\"";
        // line 167
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "assets/img/stanko.jpg\">
                            </div>
    
                            <div class=\"col-md-8\">
                                <blockquote>
                                    <i class=\"fa fa-quote-left\"></i>
                                    Kao poklon za rođendan moje devojke rešio sam da udomim jedno prase autohtone sorte Mangulice. Zahvaljujući  humanim ljudima iz UdomiMe.rs moja devojka ima novu najbolju drugaricu.
                                    <hr class=\"clients-hr\">
                                    <cite>&#8212; Stanko, automehaničar</cite>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div><!-- end of row -->
    
            </div><!-- end Jumbotron -->
    
            <div class=\"col-12 narrow text-center\">
                <p class=\"clients-text\">Udomi ne kupuj! Registracija je besplatna.</p>
                <a class=\"btn btn-secondary btn-md\" href=\"";
        // line 186
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/register\" target=\"_blank\">Registruj se</a>
            </div>
        
        </div>
    
    <!-- End Clients section -->
";
    }

    // line 194
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 195
        echo "Spisak kategorija
";
    }

    public function getTemplateName()
    {
        return "Main/home.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  297 => 195,  293 => 194,  282 => 186,  260 => 167,  239 => 149,  159 => 71,  146 => 66,  139 => 62,  135 => 61,  128 => 59,  124 => 58,  120 => 56,  116 => 55,  94 => 36,  71 => 16,  65 => 13,  59 => 10,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/home.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\Main\\home.html");
    }
}
