<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* UserDashboard/index.html */
class __TwigTemplate_138f8ddf42e606837c5aace4ce6420d00a0682852a340ed26fa133c73f915c05 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'welcome' => [$this, 'block_welcome'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "UserDashboard/index.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<div class=\"container\">
    <div class=\"row text-center\">
        <div class=\"col-md-12 my-4 naslov\">
              <h1>Opcije dostupne prijavljenim korisnicima</h1>
        </div>
    </div>  
        <div class=\"row dashboard text-center mb-5\">
            <div class=\"col mx-1 box dugme border border-success\">
                <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/profile/\" 
                class=\"btn btn-primary text-center\">
                <i class=\"fas fa-user fa-lg\"></i><br>   
                Vaš profil
                </a>
            </div>
            <div class=\"col mx-1 dugme border border-success\">
                    <a href=\"";
        // line 19
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/ads/add\"
                     class=\"btn btn-primary\">
                     <i class=\"fas fa-plus fa-lg\"></i><br>    
                     Dodavanje oglasa
                    </a>
            </div>
            <div class=\"col mx-1 box dugme border border-success\">
                <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/myads/\" 
                class=\"btn btn-primary text-center\">
                <i class=\"fas fa-paw fa-lg\"></i><br> 
                Vaši oglasi
                </a>
            </div>
            
        </div>
    
</div>
";
    }

    // line 37
    public function block_welcome($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "<div class=\"dobrodosli\">
    <p >Dobrodošli ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["item"] ?? null), "forename", [], "any", false, false, false, 39), "html", null, true);
        echo "!<i class=\"far fa-smile\" style=\"color: #20c997\"></i></p> 
</div>
";
    }

    // line 42
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "Dashboard
";
    }

    public function getTemplateName()
    {
        return "UserDashboard/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 43,  111 => 42,  104 => 39,  101 => 38,  97 => 37,  82 => 26,  72 => 19,  62 => 12,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "UserDashboard/index.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\UserDashboard\\index.html");
    }
}
