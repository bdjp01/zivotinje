<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/postRegister.html */
class __TwigTemplate_d25332ab8bc2e9df31560384e5f2db733c63b11eebc47a588983a0063df87da1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "Main/postRegister.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if (twig_test_empty(($context["message1"] ?? null))) {
            // line 5
            echo "    <div class=\"container\">
            <div class=\"card text-white bg-dark my-5\" >
                    <div class=\"row no-gutters\">
                    <div class=\"col-md-4\">
                        <img src=\"";
            // line 9
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "assets/img/warning.png\" class=\"card-img p-5\" alt=\"upozorenje\">
                    </div>
                    <div class=\"col-md-8\">
                        <div class=\"card-body mt-5\">
                        <h3 class=\"card-title\">Greška</h3>
                        <h4 class=\"card-text\">";
            // line 14
            echo twig_escape_filter($this->env, ($context["message"] ?? null));
            echo "</h4>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            
    ";
        } else {
            // line 22
            echo "    <div class=\"container\">
            <div class=\"card text-white bg-dark my-5\" >
                    <div class=\"row no-gutters\">
                    <div class=\"col-md-4\">
                        <img src=\"";
            // line 26
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "assets/img/checked.png\" class=\"card-img p-5\" alt=\"upozorenje\">
                    </div>
                    <div class=\"col-md-8\">
                        <div class=\"card-body mt-5\">
                        <h3 class=\"card-title\">Uspesno je registrovan nalog!</h3>
                        <h4 class=\"card-text\">";
            // line 31
            echo twig_escape_filter($this->env, ($context["message1"] ?? null));
            echo "</h4>
                        <a href=\"";
            // line 32
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "user/login/\"><button class=\"btn btn-primary\">
                                Prijavi se</button></a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
    ";
        }
        // line 40
        echo "
   
";
    }

    // line 44
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "Registracija
";
    }

    public function getTemplateName()
    {
        return "Main/postRegister.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 45,  114 => 44,  108 => 40,  97 => 32,  93 => 31,  85 => 26,  79 => 22,  68 => 14,  60 => 9,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/postRegister.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\Main\\postRegister.html");
    }
}
