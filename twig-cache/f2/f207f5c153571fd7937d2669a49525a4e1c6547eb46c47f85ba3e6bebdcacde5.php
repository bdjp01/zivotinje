<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/getLogin.html */
class __TwigTemplate_f7ac5b1708a180ae5abf4e2de83a297641e6b92b0eb11c3947c4924f39352844 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "Main/getLogin.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    
    <div id=\"break-sec\" class=\"offset\">
            <div class=\"col-12 narrow text-center\">
                <h1>Prijavi se</h1>
            </div>
        </div>
    <form action=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/login\" method=\"post\">
            <div class=\"form-row\">
            <div class=\"form-group col-md-6 offset-md-3\">
                <label for=\"inputUsername\">Korinsicko ime</label>
                <input type=\"text\" class=\"form-control\" id=\"inputUsername\"
                required name=\"username\" placeholder=\"Unesite korisnicko ime\">
            </div>
            </div>
        
            <div class=\"form-row\">
            <div class=\"form-group col-md-6 offset-md-3\">
                <label for=\"inputLozinka\" class=\"\">Lozinka</label>
            <div class=\"reg-password\">
                <input type=\"password\" class=\"form-control\" id=\"inputLozinka\" 
                required name=\"password\" placeholder=\"Unesite lozinku\">
            </div>
            </div>
        </div>
        
        <button type=\"submit\" class=\"btn btn-primary btn-block col-md-6 offset-md-3\">Prijavi se</button>
        
        <div class=\"col-12 narrow text-center\">
                <a href=\"";
        // line 32
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/register\">Nemam nalog / Registruj se <i class=\"fas fa-long-arrow-alt-right\"></i></a>
            </div>
            
    </form>    

";
    }

    // line 39
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "Logovanje
";
    }

    public function getTemplateName()
    {
        return "Main/getLogin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 40,  94 => 39,  84 => 32,  59 => 10,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/getLogin.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\Main\\getLogin.html");
    }
}
