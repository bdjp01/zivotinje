<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Main/getRegister.html */
class __TwigTemplate_e989de993d5ffaa307faae82fa974396b5e050c243b8b1249f0acceb4f7d502d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'naslov' => [$this, 'block_naslov'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "_global/index.html";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("_global/index.html", "Main/getRegister.html", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <!-- Start Break-sec Section -->
    <div id=\"break-sec\" class=\"offset\">
            <div class=\"col-12 narrow text-center\">
                <h1>Registruj se</h1>
            </div>
        </div>
    <!-- End Break-sec section -->
    
    
    
    <!-- Start Registracija -->
    
    <form class=\"formica\" method=\"post\">
       

      <div class=\"form-row\">
        <div class=\"form-group col-md-4 \">
          <label for=\"inputName\">Ime</label>
          <input type=\"text\" class=\"form-control\" required
            name=\"reg_forename\" id=\"inputName\" placeholder=\"Vase ime\">
        </div>
        <div class=\"form-group col-md-4\">
          <label for=\"inputName\">Prezime</label>
          <input type=\"text\" class=\"form-control\" required
          name=\"reg_surname\" id=\"inputName\" placeholder=\"Vase prezime\">
        </div>
      </div>
      
      <div class=\"form-row\">
        <div class=\"form-group col-md-8 \">
            <label for=\"inputUsername\">Username</label>
            <input type=\"text\" class=\"form-control\" required
             name=\"reg_username\" id=\"inputUsername\" placeholder=\"Vas username\">
        </div>
     </div>
    
      <div class=\"form-row\">
        <div class=\"form-group col-md-4 \">
          <label for=\"inputPassword\" class=\"\">Lozinka</label>
        <div class=\"reg-password\">
          <input type=\"password\" class=\"form-control\" required
          name=\"reg_password_1\" id=\"inputPassword\" placeholder=\"Unesite lozinku\">
        </div>
       </div>
    
       <div class=\"form-group col-md-4\">
          <label for=\"inputPassword\" class=\"\">Ponovite lozinku</label>
        <div class=\"reg-password\">
          <input type=\"password\" class=\"form-control\" required
          name=\"reg_password_2\" id=\"inputPassword\" placeholder=\"Ponovite lozinku\">
        </div>
       </div>
        
      </div>
    
      
      
      <div class=\"form-row\">
        <div class=\"form-group col-md-6 \">
          <label for=\"inputCity\">Mesto</label>
        <input type=\"text\" class=\"form-control\" required
         name=\"reg_city\" id=\"inputCity\" placeholder=\"Unesite mesto\">
        </div>
    
        <div class=\"form-group col-md-2\">
          <label for=\"inputPhone\">Telefon</label>
        <input type=\"text\" class=\"form-control\"  required
        name=\"reg_phone\" id=\"inputPhone\" placeholder=\"Unesite telefon\">
        </div>

    </div>
    <div class=\"form-row\">
      <button type=\"submit\" class=\"btn btn-primary col-md-8\" >Registruj se</button>
    </div>
    
    </form>

    <div class=\"col-12 narrow text-center\">
            <a href=\"";
        // line 82
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "user/login\">Imam nalog / Prijavi se <i class=\"fas fa-long-arrow-alt-right\"></i></a>
        </div>
";
    }

    // line 86
    public function block_naslov($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 87
        echo "Registracija
";
    }

    public function getTemplateName()
    {
        return "Main/getRegister.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 87,  138 => 86,  131 => 82,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "Main/getRegister.html", "E:\\Programi\\Xampp\\htdocs\\Zivotinje\\views\\Main\\getRegister.html");
    }
}
