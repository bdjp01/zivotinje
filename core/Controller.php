<?php
    namespace App\Core;

    use App\Core\DatabaseConnection;
    use App\Core\Session\Session;

    class Controller { 
        private $dbc;
        private $session;
        private $data = [];

        final public function __construct(DatabaseConnection &$dbc) {
            $this->dbc = $dbc;
        }

        public function setSession(Session &$session) {
            $this->session = $session;
        }

        public function &getSession(): Session {
            return $this->session;
        }

        final public function &getDatabaseConnection(): DatabaseConnection {
            return $this->dbc;
        }

        final protected function set(string $name, $value) {
            if (\preg_match('/^[a-z][a-z0-9]+(?:[A-Z][a-z0-9]+)*$/', $name)) {
                $this->data[$name] = $value;
            }
        }

        final public function getData(): array {
            return $this->data;
        }

        public function __pre() {
            
        }
    }