<?php 
    namespace App\Controllers;

    use App\Core\DatabaseConnection;
    use App\Core\Controller;
    use App\Models\AdModel;
    use App\Models\AdViewModel;

    class AdController extends Controller {
        
        public function show($id) {
            $am = new AdModel($this->getDatabaseConnection());
            $ad = $am->getById($id);

            if(!$ad) {
                header('Location: /');
                exit;
            }
            $this->set('ad', $ad);

            /*$adViewModel = new AdViewModel($this->getDatabaseConnection());
            $adViewModel->add(
                [   
                    'ad_id'      => $id,
                    'ip_address' => '199.200.10.23',
                    'user_agent' => 'chrome'
                ]
            );*/
        }
    }