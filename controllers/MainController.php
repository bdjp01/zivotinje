<?php 
    namespace App\Controllers;

    use App\Core\DatabaseConnection;
    use App\Models\CategoryModel;
    use App\Core\Controller;
    use App\Models\AdViewModel;
    use App\Models\AdModel;
    use App\Models\UserModel;
    use App\Validators\StringValidator;
    

    class MainController extends Controller {
        public function home() {
            $am = new AdModel($this->getDatabaseConnection());

            $id = rand(1,10);
            $ad = $am->getAll();
            shuffle($ad);
            $this->set('oglasi', $ad);
            
    }
    public function aboutUs() {
        
    }   

    public function getRegister() {
        
    }

    public function postRegister() {
        $username  = filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
        $city      = filter_input(INPUT_POST, 'reg_city', FILTER_SANITIZE_STRING);
        $forename  = filter_input(INPUT_POST, 'reg_forename', FILTER_SANITIZE_STRING);
        $surname   = filter_input(INPUT_POST, 'reg_surname', FILTER_SANITIZE_STRING);
        $phone     = filter_input(INPUT_POST, 'reg_phone', FILTER_SANITIZE_NUMBER_INT);
        $password1 = filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
        $password2 = filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

        if ($password1 != $password2) {
            $this->set('message', 'Morate dva puta da potvrdite istu lozinku.');
            return;
        }

        $validator = (new StringValidator())->setMinLength(6)->setMaxLength(120);
        if (! $validator->isValid($password1)) {
            $this->set('message', 'Lozinka mora imati najmanje 6 karaktera i najviše 120 karaktera.');
            return;
        }

        $um = new UserModel($this->getDatabaseConnection());

        $user = $um->getByFieldName('username', $username);
        if ($user) {
            $this->set('message', 'Korisničko ime je zauzeto.');
            return;
        }

    

        $passwrodHash = password_hash($password1, PASSWORD_DEFAULT);

        $userId = $um->add([
            'username' =>      $username,
            'password' =>      $passwrodHash,
            'forename' =>      $forename,
            'surname' =>       $surname,
            'city' =>          $city,
            'phone' =>         $phone

        ]);

        if (!$userId) {
            $this->set('message', 'Došlo je do greške prilikom registracije naloga.');
            return;
        }

        $this->set('message1', 'Možete da se prijavite sada.');
    }

    public function getLogin() {
        $this->getSession()->clear();
    }

    public function postLogin() {
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $um = new UserModel($this->getDatabaseConnection());

        $user = $um->getByFieldName('username', $username);

        if (!$user) {
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }

        if (!password_verify($password, $user->password)) {
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }

        $this->getSession()->put('userId', $user->user_id);

        \ob_clean();
        header('Location: dashboard/');
        exit;
    }

    public function getLogout() {
        $this->getSession()->clear();
        $this->getSession()->save();

        header('Location: ' . BASE . '');
    }

}