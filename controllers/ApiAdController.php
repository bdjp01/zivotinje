<?php 
    namespace App\Controllers;

    use App\Core\DatabaseConnection;
    use App\Core\ApiController;
    use App\Models\AdModel;


    class ApiAdController extends ApiController {
        public function show($id) {
            $am = new AdModel($this->getDatabaseConnection());
            $ad = $am->getById($id);
            $this->set('ad', $ad);
    }
}