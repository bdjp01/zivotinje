<?php 
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\CategoryModel;

    class CategoryController extends Controller {
        
        public function show() {
            $cm = new CategoryModel($this->getDatabaseConnection());
            $categories = $cm->getAll();
            $this->set('categories', $categories);
        }
    }