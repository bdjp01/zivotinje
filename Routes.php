<?php
    use App\Core\Route;

    return [
        #Registracija
        Route::get('#^user/register/?$#',                       'Main',                 'getRegister'),
        Route::post('#^user/register/?$#',                      'Main',                 'postRegister'),

        #Logovanje
        Route::get('#^user/login/?$#',                          'Main',                 'getLogin'),
        Route::post('#^user/login/?$#',                         'Main',                 'postLogin'),

        #Logovani korisnici
        Route::get('|^user/dashboard/?$|',                     'UserDashboard',        'index'),

        Route::get('#^categories/?$#',                          'Category',             'show'),
        Route::get('#^categories/?$#',                          'Category',             'show'),
        Route::get('|^category/([0-9]+)/delete/?$|',            'Category',             'delete'),
        Route::get('#^ads/([0-9]+)/?$#',                        'Ad',                   'show'),
        
        #Api rute
        Route::get('|^api/ad/([0-9]+)/?$|',                     'ApiAd',                'show'),


        # Fallback
        Route::get('#^.*$#',                                    'Main',                 'home')
    ];
