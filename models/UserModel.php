<?php 
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class UserModel extends Model{

        protected function getFields(): array {
            return [
                'user_id'         => new Field((new NumberValidator())->setInteger(10), false),

                'forename'        => new Field((new StringValidator())->setMaxLength(64)),
                'surname'         => new Field((new StringValidator())->setMaxLength(64)),
                'city'            => new Field((new StringValidator())->setMaxLength(64)),
                'username'        => new Field((new StringValidator())->setMaxLength(64)),
                'password'        => new Field((new StringValidator())->setMaxLength(64)),
                'phone'           => new Field((new NumberValidator())->setInteger(13))

            ];
        }
        

        public function getByUsername(string $username) {
            return $this->getByFieldName('username', $username);
        }


    }