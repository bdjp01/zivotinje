<?php 
    namespace App\Models;

    use App\Core\DatabaseConnection;
    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;


    class AdModel extends Model{
        protected function getFields(): array {
            return [
                'ad_id'         => new Field((new NumberValidator())->setInteger(10), false),

                'pet_name'      => new Field((new StringValidator())->setMaxLength(64)),
                'race'          => new Field((new StringValidator())->setMaxLength(64)),
                'gender'        => new Field((new StringValidator())->setMaxLength(64)),
                'age'           => new Field((new StringValidator())->setMaxLength(64)),
                'image_path'    => new Field((new StringValidator())->setMaxLength(64)),
                'color'         => new Field((new StringValidator())->setMaxLength(64)),
                'contact'       => new Field((new StringValidator())->setMaxLength(64)),
                'is_active'     => new Field(new BitValidator()),
                'description'   => new Field((new StringValidator())->setMaxLength(64*1024)),

                'category_id'   => new Field((new NumberValidator())->setInteger(10)),
                'user_id'       => new Field((new NumberValidator())->setInteger(10))

            ];
        }



        public function getAllByCategoryId(int $categoryId):array {
            return $this->getAllByFieldName('category_id', $categoryId);
        }

        public function getAllByAdId(int $adId):array {
            return $this->getAllByFieldName('ad_id', $adId);
        }

        public function getUserByAdId(int $id) {
            return $this->getById($id);
        }
    }